import LoginForm from "../../components/LoginForm";
import NavBar from "../../components/NavBar";

const Login = () => {
  return (
    <>
      <NavBar />
      <LoginForm></LoginForm>
    </>
  );
};

export default Login;
