import NavBar from "../../components/NavBar";
import RegisterForm from "../../components/RegisterForm";

const Register = () => {
  return (
    <>
      <NavBar />
      <RegisterForm />
    </>
  );
};

export default Register;
