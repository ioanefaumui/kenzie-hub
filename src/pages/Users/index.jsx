import AllUsers from "../../components/AllUsers";
import NavBar from "../../components/NavBar";

const Users = () => {
  return (
    <>
      <NavBar />
      <AllUsers />
    </>
  );
};

export default Users;
