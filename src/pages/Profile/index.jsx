import NavBar from "../../components/NavBar";
import UserProfile from "../../components/UserProfile";
import { useEffect, useState } from "react";
import axios from "axios";

const Profile = () => {
  return (
    <>
      <NavBar />
      <UserProfile />
    </>
  );
};

export default Profile;
