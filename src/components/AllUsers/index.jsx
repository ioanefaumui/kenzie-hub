import axios from "axios";
import { useEffect, useState } from "react";
import BlueContainer from "../BlueContainer";
import "./style.css";

const AllUsers = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    axios
      .get("https://kenziehub.me/users")
      .then((response) => setUsers(response.data));
  }, []);

  return (
    <BlueContainer>
      <div className="users-wrapper">
        {users &&
          users.map((item) => (
            <li key={item.id} className="user-card">
              <h4>{item.name}</h4>
              {item.techs &&
                item.techs.map((item) => (
                  <p style={{ color: "red" }}>
                    Techs: {item.title} {item.status}
                  </p>
                ))}
              <p style={{ fontFamily: "Quando" }}>Bio: "{item.bio}"</p>
            </li>
          ))}
      </div>
    </BlueContainer>
  );
};

export default AllUsers;
