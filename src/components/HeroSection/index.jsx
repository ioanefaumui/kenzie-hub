import { ReactComponent as Arrow } from "../../assets/arrow-right.svg";
import { Link } from "react-router-dom";
import "./style.css";
import { useState } from "react";
import { useHistory } from "react-router";

const HeroSection = () => {
  const [token] = useState(() => {
    const localToken = localStorage.getItem("token") || "";
    return localToken;
  });

  const history = useHistory();

  token && history.push("/profile");

  return (
    <>
      <section className="hero">
        <div className="hero-background" aria-hidden="true">
          <div>
            JAVASCRIPT PYTHON JAVA REACT VUE GOLANG ANGULAR C KOTLIN RUBY SQL
          </div>
          <div>
            JAVASCRIPT PYTHON JAVA REACT VUE GOLANG ANGULAR C KOTLIN RUBY SQL
          </div>
          <div>
            OBJECTIVE-C PHP SWIFT ELIXIR SCALA JAVASCRIPT PYTHON JAVA REACT VUE
          </div>
          <div>
            GOLANG ANGULAR C KOTLIN RUBY SQL OBJECTIVE-C PHP SWIFT ELIXIR SCALA
          </div>
          <div>
            JAVASCRIPT PYTHON JAVA REACT VUE GOLANG ANGULAR C KOTLIN RUBY SQL
          </div>
          <div>
            OBJECTIVE-C PHP SWIFT ELIXIR SCALA JAVASCRIPT PYTHON JAVA REACT VUE
          </div>
          <div>
            GOLANG ANGULAR C KOTLIN RUBY SQL OBJECTIVE-C PHP SWIFT ELIXIR SCALA
          </div>
          <div>
            JAVASCRIPT PYTHON JAVA REACT VUE GOLANG ANGULAR C KOTLIN RUBY SQL
          </div>
          <div>
            JAVASCRIPT PYTHON JAVA REACT VUE GOLANG ANGULAR C KOTLIN RUBY SQL
          </div>
          <div>
            JAVASCRIPT PYTHON JAVA REACT VUE GOLANG ANGULAR C KOTLIN RUBY SQL
          </div>
          <div>
            OBJECTIVE-C PHP SWIFT ELIXIR SCALA JAVASCRIPT PYTHON JAVA REACT VUE
          </div>
          <div>
            GOLANG ANGULAR C KOTLIN RUBY SQL OBJECTIVE-C PHP SWIFT ELIXIR SCALA
          </div>
          <div>
            JAVASCRIPT PYTHON JAVA REACT VUE GOLANG ANGULAR C KOTLIN RUBY SQL
          </div>
          <div>
            OBJECTIVE-C PHP SWIFT ELIXIR SCALA JAVASCRIPT PYTHON JAVA REACT VUE
          </div>
          <div>
            GOLANG ANGULAR C KOTLIN RUBY SQL OBJECTIVE-C PHP SWIFT ELIXIR SCALA
          </div>
          <div>
            JAVASCRIPT PYTHON JAVA REACT VUE GOLANG ANGULAR C KOTLIN RUBY SQL
          </div>
          <div>
            OBJECTIVE-C PHP SWIFT ELIXIR SCALA JAVASCRIPT PYTHON JAVA REACT VUE
          </div>
          <div>
            GOLANG ANGULAR C KOTLIN RUBY SQL OBJECTIVE-C PHP SWIFT ELIXIR SCALA
          </div>
          <div>
            JAVASCRIPT PYTHON JAVA REACT VUE GOLANG ANGULAR C KOTLIN RUBY SQL
          </div>
        </div>
        <main>
          <div className="hero-content">
            <h1>
              Showcase your
              <br />
              developer skills
            </h1>

            <Link to="/register" className="start-button">
              <span>Start now</span>
              <Arrow className="arrow" />
            </Link>
          </div>
        </main>
      </section>
      <div>
        <div className="shadow"></div>
        <div className="quote">“Out of sight, out of mind.”</div>
      </div>
    </>
  );
};

export default HeroSection;
