import BlueContainer from "../BlueContainer";
import "./style.css";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useHistory } from "react-router";
import axios from "axios";
import { useState } from "react";

const RegisterForm = () => {
  const [token] = useState(() => {
    const localToken = localStorage.getItem("token") || "";
    return localToken;
  });
  const [userExists, setUserExists] = useState(false);

  const history = useHistory();

  token && history.push("/profile");

  const schema = yup.object().shape({
    name: yup.string().required("name is required"),
    email: yup.string().email("Invalid e-mail").required("email is required"),
    password: yup.string().min(6).required("password is required"),
    bio: yup.string().max(116).required("tell something about you"),
    contact: yup.string().required("contact is required"),
    course_module: yup.string().required("Select a module"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const handleRegisterForm = (data) => {
    axios
      .post("https://kenziehub.me/users", data)
      .then((response) => {
        reset();
        history.push("/login");
      })
      .catch((e) => setUserExists(true));
  };
  return (
    <>
      <BlueContainer>
        <form onSubmit={handleSubmit(handleRegisterForm)}>
          <div className="columns">
            <div className="columns-wrapper">
              <div className="column c1">
                <div className="input-wrapper">
                  <input
                    type="text"
                    placeholder="Name*"
                    {...register("name")}
                  />
                  {errors.name?.message && (
                    <span className="error">{errors.name?.message}</span>
                  )}
                </div>

                <div className="input-wrapper">
                  <input
                    type="text"
                    placeholder="e-mail*"
                    {...register("email")}
                  />
                  {userExists && (
                    <span className="error">email already in use</span>
                  )}
                  {errors.email?.message && (
                    <span className="error">{errors.email?.message}</span>
                  )}
                </div>

                <div className="input-wrapper">
                  <input
                    type="password"
                    placeholder="Password*"
                    {...register("password")}
                  />
                  {errors.password?.message && (
                    <span className="error">{errors.password?.message}</span>
                  )}
                </div>
              </div>

              <div className="column c2">
                <div className="input-wrapper">
                  <textarea
                    rows="4"
                    placeholder="About me*"
                    style={{ resize: "none" }}
                    {...register("bio")}
                  ></textarea>
                  {errors.bio?.message && (
                    <span className="error">{errors.bio?.message}</span>
                  )}
                </div>

                <div className="input-wrapper">
                  <input
                    type="text"
                    placeholder="Contact (Linkedin, facebook...)*"
                    {...register("contact")}
                  />
                  {errors.contact?.message && (
                    <span className="error"> {errors.contact?.message}</span>
                  )}
                </div>

                <select
                  defaultValue=""
                  style={errors.course_module?.message && { color: "red" }}
                  className="modules"
                  {...register("course_module")}
                >
                  <option value="" disabled hidden>
                    {errors.course_module?.message
                      ? errors.course_module.message
                      : "Course module*"}
                  </option>
                  <option value="Primeiro módulo (Introdução ao Frontend)">
                    First module
                  </option>
                  <option value="Segundo módulo (Frontend Avançado)">
                    Second module
                  </option>
                  <option value="Terceiro módulo (Introdução ao Backend)">
                    Third module
                  </option>
                  <option value="Quarto módulo (Backend Avançado)">
                    Fourth module
                  </option>
                </select>
              </div>
            </div>

            <button type="submit" className="send-button">
              Send
            </button>
          </div>
        </form>
      </BlueContainer>
    </>
  );
};

export default RegisterForm;
