import { useEffect, useState } from "react";
import axios from "axios";
import NavBar from "../NavBar";
import BlueContainer from "../BlueContainer";
import "./style.css";

const UserProfile = () => {
  const [user, setUser] = useState({});
  const [token] = useState(() => {
    const localToken = localStorage.getItem("token") || "";
    return JSON.parse(localToken);
  });
  const [title, setTitle] = useState("");
  const [status, setStatus] = useState("");
  const [techError, setTechError] = useState(false);

  const onSubmit = async () => {
    await axios
      .post(
        "https://kenziehub.me/users/techs",
        {
          title: title,
          status: status,
        },
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .catch((e) => setTechError(true));
    await axios
      .get("https://kenziehub.me/profile", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => setUser(response.data));
  };

  const deleteTech = async (id) => {
    await axios
      .delete(`https://kenziehub.me/users/techs/${id}`, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .catch((e) => console.log(e));
    await axios
      .get("https://kenziehub.me/profile", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => setUser(response.data));
  };

  useEffect(() => {
    axios
      .get("https://kenziehub.me/profile", {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => setUser(response.data));
  }, []);

  return (
    <>
      <NavBar />
      <BlueContainer>
        <div className="profile-wrapper">
          <div>
            <div className="profile">🤖</div>
            <h3>
              <span style={{ color: "gray", fontWeight: "normal" }}>Name:</span>{" "}
              {user.name}
            </h3>
            <div>
              <span style={{ color: "gray", fontWeight: "normal" }}>
                Contact:
              </span>{" "}
              {user.contact}
            </div>
            <div style={{ fontFamily: "Quando", margin: "8px 0" }}>
              “{user.bio}”
            </div>
          </div>
          <div>
            {techError && (
              <span style={{ color: "red" }}>
                You already have that tech. Try another one.
              </span>
            )}
            <div className="technology">
              <input
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
              />
              <select
                defaultValue={"DEFAULT"}
                onChange={(e) => setStatus(e.target.value)}
              >
                <option value="DEFAULT" disabled hidden>
                  Level:
                </option>
                <option value="Iniciante">Begginer</option>
                <option value="Intermediário">Fluent</option>
                <option value="Avançado">Expert</option>
              </select>
              <button onClick={onSubmit}>Add technology</button>
            </div>

            <h3 style={{ margin: "16px 0" }}>Technologies:</h3>
            {user.techs &&
              user.techs.map((item) => (
                <li key={item.id}>
                  {item.title} {item.status}
                  <button
                    className="delete-button"
                    onClick={() => deleteTech(item.id)}
                  >
                    DELETE
                  </button>
                </li>
              ))}
          </div>
        </div>
      </BlueContainer>
    </>
  );
};

export default UserProfile;
