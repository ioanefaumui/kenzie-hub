import BlueContainer from "../BlueContainer";
import "./style.css";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useHistory } from "react-router";
import axios from "axios";
import { useState } from "react";

const LoginForm = () => {
  const [token] = useState(() => {
    const localToken = JSON.parse(localStorage.getItem("token")) || "";
    return localToken;
  });

  const history = useHistory();

  token && history.push("/profile");

  const schema = yup.object().shape({
    email: yup.string().email("Invalid email").required("email is required"),
    password: yup.string().required("type your kahub password"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const handleLoginForm = (data) => {
    console.log(data);
    axios
      .post("https://kenziehub.me/sessions", data)
      .then((response) => {
        localStorage.clear();
        localStorage.setItem("token", JSON.stringify(response.data.token));
        reset();
        history.push("/profile");
      })
      .catch((e) => console.log(e));
  };

  return (
    <BlueContainer>
      <form onSubmit={handleSubmit(handleLoginForm)}>
        <div className="login-wrapper">
          <div className="input-wrapper">
            <input type="text" placeholder="email*" {...register("email")} />
            {errors.email?.message && (
              <span className="error">{errors.email.message}</span>
            )}
          </div>

          <div style={{ flexDirection: "column", display: "flex" }}>
            <input
              type="password"
              placeholder="kahub password*"
              {...register("password")}
            />
            {errors.password?.message && (
              <span className="error">{errors.password?.message}</span>
            )}
          </div>

          <button type="submit" className="login-button">
            Login
          </button>
        </div>
      </form>
    </BlueContainer>
  );
};

export default LoginForm;
