import "./style.css";

const BlueContainer = ({ children }) => {
  return (
    <>
      <div className="blue-container">{children}</div>
      <div>
        <div className="shadow"></div>
        <div className="quote">“Out of sight, out of mind.”</div>
      </div>
    </>
  );
};

export default BlueContainer;
