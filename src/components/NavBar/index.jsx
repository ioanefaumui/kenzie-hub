import "./style.css";
import { Link } from "react-router-dom";
import { useState } from "react";
import { useHistory } from "react-router-dom";

const NavBar = () => {
  const [token] = useState(() => {
    const localToken = localStorage.getItem("token") || "";
    return localToken;
  });

  const history = useHistory();

  const handleLogout = () => {
    localStorage.clear();
    history.push("/");
  };

  return (
    <>
      <header className="navbar">
        <a href="/" className="logo">
          KAHUB
        </a>
        {token ? (
          <ul className="menu-links">
            <Link to="/profile">
              <button className="menu-item">Profile</button>
            </Link>
            <Link to="/users">
              <button className="menu-item ">Users</button>
            </Link>
            <Link to="/">
              <button onClick={handleLogout} className="menu-item">
                Logout
              </button>
            </Link>
          </ul>
        ) : (
          <ul className="menu-links">
            <Link to="/login">
              <button className="menu-item">Login</button>
            </Link>
            <Link to="/register">
              <button className="menu-item register">Register</button>
            </Link>
          </ul>
        )}
      </header>
    </>
  );
};

export default NavBar;
