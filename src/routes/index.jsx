import { Route, Switch } from "react-router";
import Register from "../pages/Register";
import Home from "../pages/Home";
import Login from "../pages/Login";
import UserProfile from "../components/UserProfile";
import Users from "../pages/Users";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/register">
        <Register />
      </Route>
      <Route path="/login">
        <Login />
      </Route>
      <Route path="/profile">
        <UserProfile />
      </Route>
      <Route path="/users">
        <Users />
      </Route>
    </Switch>
  );
};

export default Routes;
